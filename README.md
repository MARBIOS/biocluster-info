# MARBITS Documentation
Here you will find all the documentation aobut the **Marine Bioinformatics Platform** at ICM (MARBITS, aka Biocluster).  
Please join our [Slack Team](https://marbits.slack.com/signup).  
  
## Please proceed to the [MARBIOS wiki](https://gitlab.com/MARBIOS/biocluster-info/wikis/home) and don't forget to bookmark!  
- [Open a new issue](https://gitlab.com/MARBIOS/biocluster-info/issues/new) if you need assistance, have suggestions or have something to share.  
- [Browse existing issues](https://gitlab.com/MARBIOS/biocluster-info/issues) to know what has been going on.  

Or visit the [**FAQ**](https://gitlab.com/MARBIOS/biocluster-info/wikis/faq).  

Help us to improve this site! You can edit pages or add your own. First read the [contribution guides](https://gitlab.com/MARBIOS/biocluster-info/blob/master/CONTRIBUTING.md)  